# This downloads and builds samtools 1.8 statically.
# You may need to install some build tool dependencies for static compilation

PWD = $(shell pwd)

all: ./install/bin/samtools

./build/samtools-1.8/samtools:
	mkdir -p ./build
	wget -O ./build/samtools-1.8.tar.bz2 http://sourceforge.net/projects/samtools/files/samtools/1.8/samtools-1.8.tar.bz2/download
	tar -xjf ./build/samtools-1.8.tar.bz2 -C ./build
	cd ./build/samtools-1.8; LDFLAGS="-static -static-libstdc++ -static-libgcc" CFLAGS="-static -static-libstdc++ -static-libgcc" ./configure --prefix=$(PWD)/install --without-curses --disable-bz2 --disable-lzma
	make -C ./build/samtools-1.8

./install/bin/samtools: ./build/samtools-1.8/samtools
	mkdir -p ./install
	make -C ./build/samtools-1.8 install

clean: FORCE
	rm -rf ./build
	rm -rf ./install

FORCE:

